import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestionInterface } from 'src/app/models/QuestionInterface.model';
import { AuthService } from 'src/app/services/auth.service';
import { QuestionService } from 'src/app/services/question.service';
import { ResponseService } from 'src/app/services/response.service';

@Component({
  selector: 'app-question-details',
  templateUrl: './question-details.component.html',
  styleUrls: ['./question-details.component.scss']
})
export class QuestionDetailsComponent implements OnInit {
  public question?: QuestionInterface;
  public questionId?: string;
  isLogging: boolean = false;

  constructor(public activatedRoute: ActivatedRoute, private router: Router,
    private questionService: QuestionService, public authService: AuthService,
    private responseService: ResponseService) {
    this.questionId = this.activatedRoute.snapshot.params['id'];
    this.authService.isLogginObs().subscribe(value => this.isLogging = value);
  }

  async ngOnInit() {
    this.actualizeComponent();
  }

  async reportQuestion(tid: string) {
    await this.questionService.reportQuestion(tid);
    this.actualizeComponent();
  }

  async reportResponse(tid: string) {
    await this.responseService.reportResponse(tid);
    this.actualizeComponent();
  }

  async deleteQuestion(tid: string) {
    await this.questionService.deleteQuestion(tid);
    this.router.navigate(['/']);
  }

  async deleteResponse(tid: string) {
    await this.responseService.deleteResponse(tid);
    this.actualizeComponent();
  }

  async addVoterResponse(tid: string) {
    await this.responseService.addVoterResponse(tid);
    this.actualizeComponent();
  }

  async unVoteResponse(tid: string) {
    await this.responseService.unVoteResponse(tid);
    this.actualizeComponent();
  }

  getQRByUser(name: string){
    this.router.navigate(['homePage/', name]);
  }


  async actualizeComponent() {
    this.question = await this.questionService.getQuestionById(this.questionId!);
    this.question.creationDate = new Date(this.question.creationDate).toLocaleString();
    if (this.question.responses.length > 0) {
      this.question.responses.forEach(r => {
        r.creationDate = new Date(r.creationDate!).toLocaleString();
        if (r.voters){
          r.voters = r.voters.split(' ').length.toString();
        }
      });
      this.question.responses.sort((a, b) => Number(b.voters) -  Number(a.voters));
    }
  }
}
