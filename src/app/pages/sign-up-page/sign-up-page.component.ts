import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserInterface } from 'src/app/models/UserInterface';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-sign-up-page',
  templateUrl: './sign-up-page.component.html',
  styleUrls: ['./sign-up-page.component.scss']
})
export class SignUpPageComponent implements OnInit {

  pseudo = new FormControl('',[Validators.required]);
  mail = new FormControl('',[Validators.required]);
  password = new FormControl('',[Validators.required]);
  user?: UserInterface;
  loginForm = new FormGroup({
  });
  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  async onSubmit() {
    this.user = {
      pseudo : this.pseudo.value,
      password : this.password.value,
      mail : this.mail.value
    }
  await this.authService.addUser(this.user);
  this.router.navigate(['/']);
  }
}
