import { Injectable } from '@angular/core';
import axios from 'axios';
import { ResponseDtoInterface } from '../models/ResponseDtoInterface.model';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ResponseService {
  private apiUrl: string = 'http://localhost:8080/api/responses/';
  constructor(private authService: AuthService) { }

  public async addResponse(response: ResponseDtoInterface) {
    await axios.post(`${this.apiUrl}`, response); 
  }

  public async reportResponse(tid: string) {
    await axios.patch(`${this.apiUrl}report/` + tid ); 
  }

  public async addVoterResponse(tid: string) {
    await axios.patch(`${this.apiUrl}vote/` + tid); 
  }

  public async unVoteResponse(tid: string) {
    await axios.patch(`${this.apiUrl}unvote/` + tid); 
  }

  public async deleteResponse(tid: string) {
    await axios.delete(`${this.apiUrl}` + tid ); 
  }
}
