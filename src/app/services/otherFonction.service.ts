import { Injectable } from '@angular/core';
import axios from 'axios';
import { QRDtoInterface } from '../models/QRDtoInterface.model';
import { QuestionDtoInterface } from '../models/QuestionDtoInterface.model';
import { QuestionInterface } from '../models/QuestionInterface.model';

@Injectable({
  providedIn: 'root'
})
export class OtherFonctionService {
  private apiUrl: string = 'http://localhost:8080/api/';
  constructor() { }

  public async getQRByUser(name: string): Promise<QRDtoInterface> {
    const { data } = await axios.get(`${this.apiUrl}users/`+ name);
    return data;
  }
}
