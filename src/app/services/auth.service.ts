import { Injectable } from '@angular/core';
import axios from 'axios';
import { BehaviorSubject, Observable, of as observableOf } from 'rxjs';
import { UserInterface } from '../models/UserInterface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isLogging: BehaviorSubject<boolean>;
  private apiUrl: string = 'http://localhost:8080/api/users';
  constructor() {
    this.isLogging = new BehaviorSubject<boolean>(false);
  }

  public async auth(user: UserInterface){
    sessionStorage.setItem('session', JSON.stringify(user));
    this.isLogging.next(true);
  }

  public async addUser(user: UserInterface){
    await axios.post(`${this.apiUrl}`, user); 
    this.auth(user);
  }

  logout(){
    sessionStorage.removeItem('session');
    this.isLogging.next(false);
  }

  isLogginObs():Observable<boolean>{
    return this.isLogging.asObservable();
  }

  checkIfLog(){
    if(sessionStorage.getItem('session')){
      this.isLogging.next(true);
    }
  }

  getCurrentUserBasicAuthentication(): string {
    const currentUserPlain = sessionStorage.getItem('session')
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain)
      return "Basic " + btoa(currentUser.pseudo + ":" + currentUser.password);
    } else {
      return "";
    }
  }

  getCurrentUserName() {
    const currentUserPlain = sessionStorage.getItem('session')
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain)
      return currentUser.pseudo;
    }

  }

  // getCurrentUser() User {
  //   const currentUserPlain = sessionStorage.getItem('session')
  //   const user:UserInterface;
  //   if (currentUserPlain) {
  //     const currentUser = JSON.parse(currentUserPlain)
  //     user.pseudo = currentUser.pseudo;
  //     user.password = currentUser.password;

  //     return user;

  // }
}
