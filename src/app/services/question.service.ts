import { Injectable } from '@angular/core';
import axios from 'axios';
import { QuestionDtoInterface } from '../models/QuestionDtoInterface.model';
import { QuestionInterface } from '../models/QuestionInterface.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {
  private apiUrl: string = 'http://localhost:8080/api/questions/';
  constructor() { }

  public async getQuestions(): Promise<QuestionInterface[]> {
    const { data } = await axios.get(`${this.apiUrl}`);
    return data.map((question: any) => {
      return {
        tid: question.tid,
        title: question.title,
        content: question.content,
        creationDate: question.creationDate,
        reported: question.reported,
        language: question.language,
        responses: question.responses,
        author: question.author,
        authorEmail: question.authorEmail,
      };
    });
  }


  public async getQuestionById(id: string): Promise<QuestionInterface> {
    const { data } = await axios.get(`${this.apiUrl}` + id);
    return {
      tid: data.tid,
      title: data.title,
      content: data.content,
      creationDate: data.creationDate,
      reported: data.reported,
      language: data.language,
      responses: data.responses,
      author: data.author,
      authorEmail: data.authorEmail,
    };
  }


  public async addQuestion(question: QuestionDtoInterface) {
    await axios.post(`${this.apiUrl}`, question);
  }

  public async getQuestionsByTitle(title: string): Promise<QuestionInterface[]> {
    const { data } = await axios.get(`${this.apiUrl}`, { params: { title: title } });
    return data.map((question: any) => {
      return {
        tid: question.tid,
        title: question.title,
        content: question.content,
        creationDate: question.creationDate,
        reported: question.reported,
        language: question.language,
        responses: question.responses,
        author: question.author,
        authorEmail: question.authorEmail,
      };
    });
  }

  public async getQuestionsByLanguage(language: string): Promise<QuestionInterface[]> {
    const { data } = await axios.get(`${this.apiUrl}`, { params: { language: language } });
    return data.map((question: any) => {
      return {
        tid: question.tid,
        title: question.title,
        content: question.content,
        creationDate: question.creationDate,
        reported: question.reported,
        language: question.language,
        responses: question.responses,
        author: question.author,
        authorEmail: question.authorEmail,
      };
    });
  }

  public async getQuestionsWithoutReponse(): Promise<QuestionInterface[]> {
    const { data } = await axios.get(`${this.apiUrl}`, { params: { noResponse: 'true' } });
    return data.map((question: any) => {
      return {
        tid: question.tid,
        title: question.title,
        content: question.content,
        creationDate: question.creationDate,
        reported: question.reported,
        language: question.language,
        responses: question.responses,
        author: question.author,
        authorEmail: question.authorEmail,
      };
    });
  }

  public async reportQuestion(tid: string) {
    await axios.patch(`${this.apiUrl}` + tid);
  }

  public async deleteQuestion(tid: string) {
    await axios.delete(`${this.apiUrl}` + tid);
  }
}
