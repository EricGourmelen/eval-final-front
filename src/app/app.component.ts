import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { addHeader, treatResponse } from './component/config/Interceptor';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'finalExamFront';

  constructor(public authService: AuthService, private router: Router){
    addHeader(this.authService);
    treatResponse(this.router);
  }
}
