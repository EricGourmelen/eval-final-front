import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isLogging: boolean = false;

  constructor(public authService: AuthService){
    this.authService.isLogginObs().subscribe(value => this.isLogging = value);
  }

  ngOnInit() {
    this.authService.checkIfLog();
  }

  logout(){
    this.authService.logout();
  }

}
