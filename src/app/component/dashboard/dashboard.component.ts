import { Component } from '@angular/core';
import { QuestionInterface } from 'src/app/models/QuestionInterface.model';
import { QuestionService } from 'src/app/services/question.service';
import { FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { OtherFonctionService } from 'src/app/services/otherFonction.service';
import { QRDtoInterface } from 'src/app/models/QRDtoInterface.model';
import { ResponseInterface } from 'src/app/models/ResponseInterface.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  public questions: QuestionInterface[] = [];
  public questionsToShow: QuestionInterface[] = [];
  public responsesByUser: ResponseInterface[] = [];
  search = new FormControl('',[Validators.required]);
  isLogging: boolean = false;
  searchByUser: string = '';

  //pagination
  length = this.questions.length;
  pageSize = 5;
  startQuestion = 0;
  endQuestion = 5;

  startResponse = 0;
  endResponse = 5;
  public responsesToShow: ResponseInterface[] = [];

  constructor(private questionService: QuestionService,private otherFonctionService: OtherFonctionService,public authService: AuthService, public activatedRoute: ActivatedRoute) {
    this.authService.isLogginObs().subscribe(value => this.isLogging = value);
    this.searchByUser = this.activatedRoute.snapshot.params['name'];
  }

  async ngOnInit() {
    this.loadAllQuestions();
    this.searchByUser = await this.activatedRoute.snapshot.params['name'];
    if(this.searchByUser){
      this.getQRByUser(this.searchByUser);
    }
  }

  async loadAllQuestions(){
    this.searchByUser = '';
    this.questions = await this.questionService.getQuestions();
    this.orderQuestionByDate();
    this.displayQuestion();
    this.startQuestion = 0;
    this.endQuestion = 5;
  }

  async onSubmit(){
    this.questions = await this.questionService.getQuestionsByTitle(this.search.value);
    this.orderQuestionByDate();
    this.displayQuestion();
  }

  async onSelectLanguage(language: string){
    this.questions = await this.questionService.getQuestionsByLanguage(language);
    this.orderQuestionByDate();
    this.displayQuestion();
    this.startQuestion = 0;
    this.endQuestion = 5;
  }

  async getQuestionsWithoutReponse(){
    this.questions = await this.questionService.getQuestionsWithoutReponse();
    this.orderQuestionByDate();
    this.displayQuestion();
    this.startQuestion = 0;
    this.endQuestion = 5;
  }

  orderQuestionByDate(){
    this.questions.sort((a, b) => new Date(b.creationDate!).getTime() - new Date(a.creationDate!).getTime());
    this.questions.forEach(q => {
      q.creationDate = new Date(q.creationDate!).toLocaleString();
    });
  }

  async reportQuestion(tid: string){
    await this.questionService.reportQuestion(tid);
    this.displayQuestion();
  }

  async deleteQuestion(tid: string){
    await this.questionService.deleteQuestion(tid);
    this.loadAllQuestions();
  }

  async getQRByUser(name : string){
    const qRByUser: QRDtoInterface = await  this.otherFonctionService.getQRByUser(name);
    this.searchByUser = name;
    this.questions = qRByUser.questions;
    this.responsesByUser = qRByUser.responses;
    this.displayQuestion();
  }

  nextQuestions(){
    if(this.startQuestion + 5 > this.questions.length - 5){
      this.startQuestion = this.questions.length - 5
    }else {
    this.startQuestion + 5;
    }
    if(this.endQuestion + 5 > this.questions.length){
      this.endQuestion = this.questions.length
    }else {
      this.endQuestion = this.endQuestion + 5;
    }
    this.displayQuestion();
  }
  previousQuestions(){
    if(this.startQuestion - 5 < 1){
      this.startQuestion = 0;
    }else {
    this.startQuestion - 5;
    }
    if(this.endQuestion - 5 < 5){
      this.endQuestion = 5
    }else {
      this.endQuestion = this.endQuestion - 5;
    }
    this.displayQuestion();
  }

  displayQuestion(){
    this.questionsToShow = [];
    let counter = 0;
    for (let index = this.startQuestion; index < this.endQuestion; index++) {
      if(index < this.questions.length){
        this.questionsToShow[counter] = this.questions[index];
      }
      counter++;
    }
  }

  nextResponses(){
    if(this.startResponse + 5 > this.responsesByUser.length - 5){
      this.startResponse = this.responsesByUser.length - 5
    }else {
    this.startResponse + 5;
    }
    if(this.endResponse + 5 > this.responsesByUser.length){
      this.endResponse = this.responsesByUser.length
    }else {
      this.endResponse = this.endResponse + 5;
    }
    this.displayResponses();
  }
  previousResponses(){
    if(this.startResponse - 5 < 1){
      this.startResponse = 0;
    }else {
    this.startResponse - 5;
    }
    if(this.endResponse - 5 < 5){
      this.endResponse = 5
    }else {
      this.endResponse = this.endResponse - 5;
    }
    this.displayResponses();
  }

  displayResponses(){
    this.responsesToShow = [];
    let counter = 0;
    for (let index = this.startResponse; index < this.endResponse; index++) {
      if(index < this.responsesByUser.length){
        this.responsesToShow[counter] = this.responsesByUser[index];
      }
      counter++;
    }
  }

}
