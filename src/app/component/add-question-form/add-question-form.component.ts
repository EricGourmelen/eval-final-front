import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, ReactiveFormsModule  } from '@angular/forms';
import { Router } from '@angular/router';
import { QuestionDtoInterface } from 'src/app/models/QuestionDtoInterface.model';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-add-question-form',
  templateUrl: './add-question-form.component.html',
  styleUrls: ['./add-question-form.component.scss']
})
export class AddQuestionFormComponent implements OnInit {
  public newQuestion?: QuestionDtoInterface;
  languageList = [
    {name:'JAVA'},
    {name :'PHP'},
    {name :'JAVASCRIPT'},
    {name :'TYPESCRIPT'}]
 
  
  questionForm = new FormGroup({
    title : new FormControl('',[Validators.required]),
    content : new FormControl('',[Validators.required]),
    language : new FormControl(this.languageList[0],[Validators.required]),
  });

  constructor(private questionService: QuestionService,private router: Router) { }

  ngOnInit(): void {
  }

  async onSubmit() {
    this.newQuestion = {  
      title : this.questionForm.value.title,
      content : this.questionForm.value.content,
      language : this.questionForm.value.language.name
    }
    await this.questionService.addQuestion(this.newQuestion);
    this.router.navigate(['/']);
  }

}
