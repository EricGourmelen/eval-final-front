import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddResponseFormComponent } from './add-response-form.component';

describe('AddResponseFormComponent', () => {
  let component: AddResponseFormComponent;
  let fixture: ComponentFixture<AddResponseFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddResponseFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddResponseFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
