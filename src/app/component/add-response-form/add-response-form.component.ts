import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ResponseDtoInterface } from 'src/app/models/ResponseDtoInterface.model';
import { ResponseService } from 'src/app/services/response.service';

@Component({
  selector: 'app-add-response-form',
  templateUrl: './add-response-form.component.html',
  styleUrls: ['./add-response-form.component.scss']
})
export class AddResponseFormComponent implements OnInit {
  public newResponse?: ResponseDtoInterface;
  @Input()
  public questionTid?: string; 
  content = new FormControl('',[Validators.required]);
  @Output() 
  refreshParent = new EventEmitter<boolean>();

  responseForm = new FormGroup({
  });

  constructor(private responseService: ResponseService, private router: Router) { }

  ngOnInit(): void {
  }

  async onSubmit() {
    this.newResponse = {  
      content : this.content.value,
      questionTid: this.questionTid!
    }
    await this.responseService.addResponse(this.newResponse);
    this.content.reset();
    this.refreshParent.emit(true);
  }

}
