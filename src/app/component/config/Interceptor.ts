import { Router } from "@angular/router";
import axios, { AxiosError, AxiosRequestConfig } from "axios"
import { AuthService } from "src/app/services/auth.service";

export const addHeader = (authService: AuthService):void =>{
    axios.interceptors.request.use(
      (config: AxiosRequestConfig) => {
        if(config.headers && config.method !== 'get')
        config.headers['Authorization'] = authService.getCurrentUserBasicAuthentication()
        return config
      },
      (error: AxiosError) => {
        console.error('ERROR:', error)
        Promise.reject(error)
      })
  }

  export const treatResponse = (router: Router):void =>{
    axios.interceptors.response.use(response => {
      console.log("no error");
      return response;
   }, error => {
     if (error.response.status === 401) {
       console.log("401 => login");
      router.navigate(['/login']);
     }
     throw error;
   });
  }
