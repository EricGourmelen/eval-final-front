
import { QuestionInterface } from "./QuestionInterface.model";
import { ResponseInterface } from "./ResponseInterface.model";

export interface QRDtoInterface {
    questions: QuestionInterface[];
    responses: ResponseInterface[];
}