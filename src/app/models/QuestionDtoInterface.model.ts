import { ResponseInterface } from "./ResponseInterface.model";

export interface QuestionDtoInterface {
    title: string;
    content: string;
    language: string;
}