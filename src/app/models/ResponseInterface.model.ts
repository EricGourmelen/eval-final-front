export interface ResponseInterface {
	tid: string;
    content: string;
	creationDate: string;
	reported: boolean;
	voters: string;
	author: string;
	authorEmail: string;

}