export interface ResponseDtoInterface {
    content: string;
    questionTid: string;
}