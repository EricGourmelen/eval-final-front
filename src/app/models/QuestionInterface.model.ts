import { ResponseInterface } from "./ResponseInterface.model";

export interface QuestionInterface {
    tid: string;
    title: string;
    content: string;
	creationDate: string;
	reported: boolean;
    language: string;
    author: string;
    authorEmail: string;
    responses: ResponseInterface[];
}